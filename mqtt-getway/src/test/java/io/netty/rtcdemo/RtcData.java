package io.netty.rtcdemo;

import java.util.Map;

import com.alibaba.fastjson.JSON;

public class RtcData {

	
	private String eventName;
	private Map<String,Object> data;
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	
	
	
	@Override
	public String toString() {
		return "RtcData [eventName=" + eventName + ", data=" + data + "]";
	}
	public static void main(String[] args) {
		
		
		
		
		String s="{\"eventName\":\"__join\",\"data\":{\"room\":\"default\"}}";
		
		RtcData data=JSON.parseObject(s, RtcData.class);
		System.out.println(data);
	}
}
